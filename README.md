# Download 

git clone https://gitlab.cern.ch/vhegde/rpi-daq-analyzer.git

# Prerequisites

cmake, c++11, root, boost

# Installation

```
mkdir build
cd build
cmake ../
make install -j4
cd ..
```

# Running the analyzer
Print all available options:
```
./bin/hexaboardAnalyzer --help
```

Running analysis on raw data file (6inch modules):
```
./bin/hexaboardAnalyzer --fileName=./Module95_6-3-2019_14-22.raw --compressedData=1 --headerSize=192 --outputName=module95 --maxTS=5 -m chanID_to_pad.txt -s 1 --hexaboardType=6inch
```

Running analysis on raw data file (8inch modules):
```
./bin/hexaboardAnalyzer --fileName=./ModuleXXX.raw --compressedData=1 --headerSize=192 --outputName=moduleXXX --maxTS=5 -m pad_to_channel_map_8inch_198.csv -s 1 --hexaboardType=8inch
```

The analyzer creates 1 root file with 1 TTree containing the raw data from the Skiroc2-CMS chip and 4 txt files containing the pedestal and noise values for all channels (also including the non-connected channels) for both gain. The format of the txt files is
`
chipID channelID value
`
where `value` can be pedestal or noise for both low and high gain.