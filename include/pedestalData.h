#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <utility>
#include <iomanip>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>

#include "skiroc2cms.h"

class pedestalChanData{
 public:
  pedestalChanData(){;}
  pedestalChanData(int chip, int channel);
  pedestalChanData(int chip, int channel, int sensorpad);
  
  inline int chip()      const { return m_chip; }
  inline int channel()   const { return m_channel; }
  inline int sensorpad() const { return m_sensorpad; }
  
  float getPedestal(int gain,int sca)             	{ return gain==1 ? m_pedestalHG[sca] : m_pedestalLG[sca]; }
  float getNoise   (int gain,int sca)             	{ return gain==1 ? m_noiseHG[sca]    : m_noiseLG[sca];    }
  void setPedestal(int gain,int sca, float value)	{ gain==1 ? m_pedestalHG[sca]=value  : m_pedestalLG[sca]=value; }
  void setNoise   (int gain,int sca, float value)	{ gain==1 ? m_noiseHG[sca]=value     : m_noiseLG[sca]=value;    }
  
  bool operator==(const pedestalChanData& peddata)
  {
    return m_chip==peddata.chip() && m_channel==peddata.channel() ;
  }
  
  friend std::ostream& operator<<(std::ostream& out,const pedestalChanData& p)
  {
    out << "Chip,channel,sensorpad " << std::dec << p.m_chip << " " << p.m_channel << " " << p.m_sensorpad << "\t";
    out << std::setprecision(3) ;
    for(int i=0; i<NUMBER_OF_SCA; i++)
      out << " " << p.m_pedestalHG[i] ;
    out << " ";
    for(int i=0; i<NUMBER_OF_SCA; i++)
      out << " " << p.m_noiseHG[i] ;
    out << "\t";
    for(int i=0; i<NUMBER_OF_SCA; i++)
      out << " " << p.m_pedestalLG[i] ;
    out << " ";
    for(int i=0; i<NUMBER_OF_SCA; i++)
      out << " " << p.m_noiseLG[i] ;
    return out;
  }

 protected:
  int m_chip;
  int m_channel;
  int m_sensorpad;
  std::vector<float> m_pedestalHG;
  std::vector<float> m_pedestalLG;
  std::vector<float> m_noiseHG;
  std::vector<float> m_noiseLG;

 private:
  friend class boost::serialization::access;
  template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
      ar & m_chip;
      ar & m_channel;
      ar & m_sensorpad;
      ar & m_pedestalHG;
      ar & m_noiseHG;
      ar & m_pedestalLG;
      ar & m_noiseLG;
    }
  
};

class pedestalData{
 public:
  pedestalData(){;}
  void fillData(pedestalChanData pcd){ m_datas.push_back(pcd); }
  void fillData(int chip, int channel, int sensorpad);
  void update(pedestalChanData pcd);
  pedestalChanData&              get(int chip,int channel);
  std::vector<pedestalChanData>& get(){return m_datas;}
  bool                           contain(int chip,int channel);
  
 protected:
  std::vector<pedestalChanData> m_datas;

 private:
  friend class boost::serialization::access;
  template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
      ar & m_datas;
    }
  
};

class pedestalArchiver
{
 public:
  pedestalArchiver(){;}
  static void savePedestalData( const pedestalData &data, const std::string output)
  {
    std::ofstream ofs(output);
    boost::archive::binary_oarchive oa(ofs);
    oa << data;
  }  
  static void loadPedestalData( const std::string input , pedestalData &data)
  {
    std::ifstream ifs( input );
    boost::archive::binary_iarchive ia(ifs);
    ia >> data;
  }  
};

